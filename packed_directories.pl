#!/usr/bin/perl

use utf8;
use strict;
use warnings;

# Capture dir choice from args
my $dir = $ARGV[0]; 

my @artists;
my @repeated;

# Scan list of dirs in the passed directory
sub scanDir {
	opendir(DIR, $dir) or die $!;
	for my $line (readdir(DIR)) {

		# Process line, trimming spaces, making all chars lower case and removing underlines
		my @line = split /-|\(|\[/, $line;
		my $artist = lc($line[0]);
		$artist =~ s/^\s+|\s+$//g;
		$artist =~ s/_/ /g;
		#print "artist $artist\n";
		push @artists, $artist;
	}
	@artists = sort @artists;
	closedir(DIR);

	return @artists;
}

# Check for duplicated artists in the array 
## TODO: implement the possibility to choose a minimum number of albums to pack in a new dir
sub findDuplicates {
	my $old = '';
	for my $curArtist (@_) {
		unless($old eq '') {
			if($old eq $curArtist) {
				unless($curArtist ~~ @repeated){
					push @repeated, $curArtist;
				}
			}
		}

		$old = $curArtist;
	}
	return @repeated;
}

# For each duplicated artist found, create new dir named after the artist name
## Account for cases where dir already exists
sub dirCreation {
	for my $artist (@_) {
		$artist =~ s/([\w']+)/\u\L$1/g; # Capitalize words
		$artist =~ s/\s/\\ /g; # Escape white space
		my $path = $dir . $artist;
		#print " creating new directory $path ... \n";
		#qx|mkdir $path|;
		mvRepeated($artist);
	}

	return 1;
}

# Move each folder that matches the repeated artists name and their content to the newly created dir
sub mvRepeated {
	my $artist = $_[0];
	#print "mvRepeated cur Artist $artist\n";

	# Scan dir looking for matches for current artist
	opendir(DIR, $dir) or die $!;
	for my $line (readdir(DIR)) {

		# Check for match in line
		if ($line =~ /$artist/) {
			$artist =~ s/\s/\ /g; # Escape white space
			$line =~ s/\s/\\ /g; # Escape white space
			my $origin = $dir . $line;
			my $destination = $dir . $artist;
			qx|mv $origin $destination|;
			print "moving $origin to $destination ...\n";
		}
	}

	closedir(DIR);
}

scanDir($dir);
findDuplicates(@artists);
dirCreation(@repeated);
